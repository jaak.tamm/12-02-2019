function replicate(word, times) {
    if (times <= 0) {
        return '';
    }
    if (!Number.isInteger(times)) {
        throw Error('times have to be int!');
    }
    if (typeof word !== 'string') {
        throw Error('word is not string!');
    } 
    return word.repeat(times);
}

module.exports = replicate;